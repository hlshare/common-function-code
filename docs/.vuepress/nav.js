module.exports = [
  {
    text: '首页',
    link: '/',
  },
  {
    text: '方法函数',
    link: '/util/',
  },
  {
    text: '组件',
    link: '/component/',
  },
]
