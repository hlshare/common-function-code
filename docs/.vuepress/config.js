module.exports = {
  title: '重拙前端物料库',
  description: '一步一步建设中',
  dest: './dist',
  port: '7777',
  head: [['link', { rel: 'icon', href: '/favicon.ico' }]],
  markdown: {
    lineNumbers: true,
  },
  plugins: [['vuepress-plugin-code-copy', true]],
  themeConfig: {
    nav: require('./nav'),
    sidebar: require('./sidebar'),
    sidebarDepth: 2,
    lastUpdated: 'Last Updated',
    searchMaxSuggestoins: 10,
    serviceWorker: {
      updatePopup: {
        message: 'New content is available.',
        buttonText: 'Refresh',
      },
    },
    editLinks: false,
    // editLinkText: '在 GitHub 上编辑此页 ！',
  },
}
