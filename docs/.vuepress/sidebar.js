module.exports = {
  '/util/': [
    // {
    //   title: '方法函数',
    //   sidebarDepth: 2,
    //   children: [
    //     {
    //       title: '介绍',
    //       path: '/util/',
    //     }
    //   ],
    // },
    {
      title: '介绍',
      path: '/util/',
    },
    {
      title: '时间相关',
      path: '/util/time',
    },
    {
      title: '数组相关',
      path: '/util/arr',
    },
    {
      title: '对象相关',
      path: '/util/obj',
    },
    {
      title: '验证相关',
      path: '/util/reg',
    },
    {
      title: '脱敏相关',
      path: '/util/des'
    },
    {
      title: '存储相关',
      path: '/util/storage'
    },
    {
      title: '运算相关',
      path: '/util/cal'
    },
    {
      title: '优化相关',
      path: '/util/optimization'
    },
  ],
  '/component/': [
    {
      title: '介绍',
      path: '/component/',
    },
    {
      title: 'table表格',
      path: '/component/table'
    },
  ]
}
