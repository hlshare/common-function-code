# 关于时间的方法

## 时间戳转为 "YYYY-MM-DD HH:mm:ss" 返回时间

```javascript
// 返回时间 时间戳转为 "YYYY-MM-DD HH:mm:ss"
export const returnTime = (time) => {
  if (!time) {
    return ''
  }
  let date = new Date(time)
  let y = date.getFullYear()
  let MM = date.getMonth() + 1
  MM = MM < 10 ? '0' + MM : MM
  let d = date.getDate()
  d = d < 10 ? '0' + d : d
  let h = date.getHours()
  h = h < 10 ? '0' + h : h
  let m = date.getMinutes()
  m = m < 10 ? '0' + m : m
  let s = date.getSeconds()
  s = s < 10 ? '0' + s : s
  return y + '-' + MM + '-' + d + ' ' + h + ':' + m + ':' + s
}
```

## "YYYY-MM-DD HH:mm:ss"转为时间戳 返回日期

```javascript
// 返回日期 "YYYY-MM-DD HH:mm:ss"转为时间戳
export const returnTimeStamp = (time) => {
  if (!time) {
    return null
  }
  var timestamp2 = Date.parse(new Date(time))
  return timestamp2
}
```

## 时间格式化、1 位数时，前面拼接 0

```javascript
// 时间格式化、1位数时，前面拼接0
export const spliceZero = (i) => {
  if (i.toString().length === 1) {
    i = '0' + i
  } else {
    i = i + ''
  }
  return i
}
```

## 获取当前日期 yy-mm-dd
```javascript
// 获取当前日期yy-mm-dd
// date 为时间对象
export const getNowTime = (date) => {
var dt = date
return dt.getFullYear() + '-' + spliceZero(dt.getMonth() + 1) + '-' + spliceZero(dt.getDate())
}
```
## 计算两个时间戳之间的时间差 X天Y小时Z分Q秒
```javascript
/**
 * @description 计算两个时间戳之间的时间差 X天Y小时Z分Q秒
 * @param {Timestamp} start 开始时间戳
 * @param {Timestamp} end 结束时间戳
 */
export const timeCal = (start, end) => {
  //如果时间格式是正确的，那下面这一步转化时间格式就可以不用了
  var dateDiff = end - start;//时间差的毫秒数
  var dayDiff = Math.floor(dateDiff / (24 * 3600 * 1000));//计算出相差天数
  var leave1=dateDiff%(24*3600*1000)    //计算天数后剩余的毫秒数
  var hours=Math.floor(leave1/(3600*1000))//计算出小时数
  //计算相差分钟数
  var leave2=leave1%(3600*1000)    //计算小时数后剩余的毫秒数
  var minutes=Math.floor(leave2/(60*1000))//计算相差分钟数
  //计算相差秒数
  var leave3=leave2%(60*1000)      //计算分钟数后剩余的毫秒数
  var seconds=Math.round(leave3/1000)

  var timeCal = (dayDiff ? dayDiff+"天" : '')+hours+"小时"+minutes+"分"+seconds+"秒"
  return timeCal
}
```
## 时间Date转为 "YYYY-MM-DD"
```javascript
// 返回日期 时间Date转为 "YYYY-MM-DD"
export const returnDateDay = (date) => {
  if (!date) {
    return ''
  }
  let y = date.getFullYear()
  let MM = date.getMonth() + 1
  MM = MM < 10 ? ('0' + MM) : MM
  let d = date.getDate()
  d = d < 10 ? ('0' + d) : d
  return y + '-' + MM + '-' + d
}
```

## 获取几天前日期
```javascript
// 获取几天前日期 getDay(-7) 7天前日期  getDay(0) 当天日期
export const getDay = (day) => {
  var today = new Date()
  var targetday_milliseconds = today.getTime() + 1000 * 60 * 60 * 24 * day
  today.setTime(targetday_milliseconds) // 注意，这行是关键代码
  var tYear = today.getFullYear()
  var tMonth = today.getMonth()
  var tDate = today.getDate()
  tMonth = spliceZero(tMonth + 1)
  tDate = spliceZero(tDate)
  return tYear + '-' + tMonth + '-' + tDate
}
```
## 获得相对当月N个月的起止日期
```javascript
/** 
* 获得相对当月AddMonthCount个月的起止日期 
* AddMonthCount为0 代表当月 为-1代表上一个月 为1代表下一个月 以此类推
* ***/
export const getMonthStartAndEnd = (AddMonthCount)=> {
	//起止日期数组  
	var startStop = new Array(); 
	//获取当前时间  
	var currentDate = new Date();
	var month=currentDate.getMonth()+AddMonthCount;
	if(month<0){
	  var n = parseInt((-month)/12);
	  month += n*12;
	  currentDate.setFullYear(currentDate.getFullYear()-n);
	}
	currentDate = new Date(currentDate.setMonth(month));
	//获得当前月份0-11  
	var currentMonth = currentDate.getMonth(); 
	//获得当前年份4位年  
	var currentYear = currentDate.getFullYear(); 
	//获得上一个月的第一天  
	var currentMonthFirstDay = new Date(currentYear, currentMonth,1); 
	//获得上一月的最后一天  
	var currentMonthLastDay = new Date(currentYear, currentMonth+1, 0); 
	//添加至数组  
	startStop.push(getDateStr3(currentMonthFirstDay)); 
	startStop.push(getDateStr3(currentMonthLastDay)); 
	//返回  
	return startStop; 
}
//date 为时间对象
function getDateStr3(date) {
	var year = "";
	var month = "";
	var day = "";
	var now = date;
	year = ""+now.getFullYear();
	if((now.getMonth()+1)<10){
	  month = "0"+(now.getMonth()+1);
	}else{
	  month = ""+(now.getMonth()+1);
	}
	if((now.getDate())<10){
	  day = "0"+(now.getDate());
	}else{
	  day = ""+(now.getDate());
	}
	return year+"-"+month+"-"+day;
}
```

## 小时转分钟
```javascript
// 小时转分钟
export function timeTominutes (time, minutes) {
  return hours * 60 + minutes
}
```
## 分钟转小时
```javascript
// 分钟转小时
export function minutesTotime (minutes, split) {
  let hour = parseInt(minutes / 60)
  let minute = minutes % 60
  hour < 10 && (hour = '0' + hour)
  minute < 10 && (minute = '0' + minute)
  return hour + split + minute
}
```
## 分转元
```javascript
// 分转元
export function fenToyuan (fen, precision) {
  if (!fen && fen !== 0) {
    return null
  }
  return ((fen || 0) / 100).toFixed(precision || 2)
}
```
## 元转分
```javascript
// 元转分
export function yuanTofen (yuan) {
  if (!yuan && yuan !== 0) {
    return null
  }
  return parseInt((yuan || 0) * 100)
}
```

## 货币转换到小数点后两位
```javascript
// 货币转换到小数点后两位
export function formatMoney (value) {
  if(value && value !==0){
    return parseFloat(value / 100).toFixed(2)
  }else if(value==0){
    return '0.00'
  }else{
    return 0
  }
}
```

## 总秒数转化为时分秒
```javascript
export function formatTimes (value){
  let result = parseInt(value)
  let h = Math.floor(result / 3600) < 10 ? '0' + Math.floor(result / 3600) : Math.floor(result / 3600)
  let m = Math.floor((result / 60 % 60)) < 10 ? '0' + Math.floor((result / 60 % 60)) : Math.floor((result / 60 % 60))
  let s = Math.floor((result % 60)) < 10 ? '0' + Math.floor((result % 60)) : Math.floor((result % 60))
  result = `${h}时${m}分${s}秒`
  return result
}
```