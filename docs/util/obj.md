# 关于对象的方法

## 比较 2 个对象是否相同

```javascript
/**
 * @description 比较2个对象是否相同
 * @param {Object} o1 对象1
 * @param {Object} o2 对象2
 */
export function isObjEqual(o1, o2) {
  const props1 = Object.getOwnPropertyNames(o1)
  const props2 = Object.getOwnPropertyNames(o2)
  if (props1.length != props2.length) {
    return false
  }
  for (let i = 0; i < props1.length; i++) {
    let propName = props1[i]
    if (o1[propName] !== o2[propName]) {
      return false
    }
  }
  return true
}
```

## 对象深拷贝

```javascript
/**
 * @param {*} newObj 拷贝对象
 * @param {*} obj2 被拷贝对象
 * @description 深拷贝
 */
export const deepCopy = (newObj, oldObj) => {
  for (let k in oldObj) {
    let item = oldObj[k]
    if (item instanceof Array) {
      newObj[k] = []
      deepCopy(newObj[k], item)
    } else if (item instanceof Object) {
      newObj[k] = {}
      deepCopy(newObj[k], item)
    } else {
      newObj[k] = item
    }
  }
}
```

## 判断对象是否包含某属性

```javascript
/**
 * 判断一个对象是否存在key，如果传入第二个参数key，则是判断这个obj对象是否存在key这个属性
 * 如果没有传入key这个参数，则判断obj对象是否有键值对
 */
export const hasKey = (obj, key) => {
  if (key) return key in obj
  else {
    let keysArr = Object.keys(obj)
    return keysArr.length
  }
}
```

## data 转为 params 参数

```javascript
export function obj2Params(data) {
  let result = ''
  const keys = Object.keys(data)
  keys.forEach((key) => {
    result += '&' + key + '=' + data[key]
  })

  return result.substr(1)
}
```
