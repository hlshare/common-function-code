# 关于存储的方法

## 获取url参数
```javascript
// 获取url参数
export const queryURLParams = (url) => {
  let result = {}
  const reg1 = /([^?=&#]+)=([^?=&#]+)/g
  const reg2 = /#([^?=&#]+)/g
  url.replace(reg1, (n, x, y) => result[x] = y)
  url.replace(reg2, (n, x) => result['HASH'] = x)
  return result
}

/**
 * @param {String} url
 * @description 从URL中解析参数
 */
export const getParams = url => {
  const keyValueArr = url.split('?')[1].split('&')
  let paramObj = {}
  keyValueArr.forEach(item => {
    const keyValue = item.split('=')
    paramObj[keyValue[0]] = keyValue[1]
  })
  return paramObj
}

// url后面取token
export const getHrefToken = () => {
  // const token = Cookies.get(TOKEN_KEY)
  const thisUrl = window.location.href
  const query = queryURLParams(thisUrl)
  const token = query.token
  if (token) return token
  else return false
}
```