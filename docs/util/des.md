# 关于脱敏的方法

## 卡号中间7位加密
```javascript
/**
 * @description 卡号中间7位加密
 * @param {card} 卡号
 */
export const encrypteCard = (card) => {
  card = "" + card;
  card= card.substr(0,8) + "******" + card.substr(14)
  return card
}
```
## 手机号码中间4位加密
```javascript
/**
 * @description 手机号码中间4位加密
 * @param {phone} 手机号
 */
export const encryptePhone = (phone) => {
  phone = "" + phone;
  phone= phone.substr(0,3) + "****" + phone.substr(7)
  return phone
}
```
## 数据脱敏(适用于所有)
```javascript
// 数据脱敏  身份证1***11111111*****1  其他***111*** 要求参数_this为string类型
// js数字最多为16位，超出部分将自动变为0
export const dataDesensitization = (_this) => {
  let text = '-'
  if (_this) {
    const length = _this.length
    if (length == 18) {
      text = _this.substring(0, 1) + '***' + _this.substring(4, 12) + '*****' + _this.substring(17, 18)
    } else {
      let len = ''
      let i = 1
      while (i <= length) {
        len = len + '*'
        i++
      }
      const pre = Number(length / 3)
      text = len.substring(0, pre) + _this.substring(pre, pre * 2) + len.substring(pre * 2, Number(length))
    }
  }
  return text
}
```