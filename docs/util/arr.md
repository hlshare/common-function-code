# 关于数组的方法

## 数组1去除数组2
```javascript
/**
 * @description 数组1去除 数组2
 * @param {Array} 数组1
 * @param {Array} 数组2
 * @param {String} name 去重依据属性
 */
export const twoArrObjDeduplication = (array1, array2, name) => {
  const a = array1
  const b = array2
  const c = a.filter(x => b.every(y => y[name] !== x[name]))
  return c
}
```
## 取两数组对象中相同值
```javascript
/**
 * @description 取两数组对象中相同值
 * @param {Array} array1 数组对象
 * @param {Array} array2 数组对象
 * @param {String} name 去重依据属性
 */
export const arrGetSameValue = (array1, array2, name) => {
  var result = []
  for (var i = 0; i < array2.length; i++) {
    var obj = array2[i]
    var num = obj[name]
    var isExist = false
    for (var j = 0; j < array1.length; j++) {
      var aj = array1[j]
      var n = aj[name]
      if (n === num) {
        isExist = true
        break
      }
    }
    if (isExist) {
      result.push(obj)
    }
  }
  return result
}
```
## 数组对象去重
```javascript
/**
 * @description 对象数组去重（依据name属性相同值去重）
 * @param {Array} arr 对象数组
 * @param {String} name 去重依据属性
 */
export const arrObjDeduplication = (arr, name) => {
  let map = new Map()
  for (let item of arr) {
    if (!map.has(item[name])) {
      map.set(item[name], item)
    }
  }
  return [...map.values()]
}
```