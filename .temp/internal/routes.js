/**
 * Generated by "@vuepress/internal-routes"
 */

import { injectComponentOption, ensureAsyncComponentsLoaded } from '@app/util'
import rootMixins from '@internal/root-mixins'
import GlobalLayout from "D:\\myPractice\\vue\\vuePress\\node_modules\\@vuepress\\core\\lib\\client\\components\\GlobalLayout.vue"

injectComponentOption(GlobalLayout, 'mixins', rootMixins)
export const routes = [
  {
    name: "v-7366a926",
    path: "/component/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-7366a926").then(next)
    },
  },
  {
    path: "/component/index.html",
    redirect: "/component/"
  },
  {
    name: "v-83db2156",
    path: "/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-83db2156").then(next)
    },
  },
  {
    path: "/index.html",
    redirect: "/"
  },
  {
    name: "v-11c739d9",
    path: "/component/table.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-11c739d9").then(next)
    },
  },
  {
    name: "v-70338038",
    path: "/util/",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-70338038").then(next)
    },
  },
  {
    path: "/util/index.html",
    redirect: "/util/"
  },
  {
    name: "v-755f4bd7",
    path: "/util/arr.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-755f4bd7").then(next)
    },
  },
  {
    name: "v-432284bd",
    path: "/util/cal.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-432284bd").then(next)
    },
  },
  {
    name: "v-08d71735",
    path: "/util/des.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-08d71735").then(next)
    },
  },
  {
    name: "v-22700b93",
    path: "/util/optimization.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-22700b93").then(next)
    },
  },
  {
    name: "v-16b083b1",
    path: "/util/reg.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-16b083b1").then(next)
    },
  },
  {
    name: "v-7b0f7bba",
    path: "/util/storage.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-7b0f7bba").then(next)
    },
  },
  {
    name: "v-7349d32b",
    path: "/util/obj.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-7349d32b").then(next)
    },
  },
  {
    name: "v-9c4798da",
    path: "/util/time.html",
    component: GlobalLayout,
    beforeEnter: (to, from, next) => {
      ensureAsyncComponentsLoaded("Layout", "v-9c4798da").then(next)
    },
  },
  {
    path: '*',
    component: GlobalLayout
  }
]